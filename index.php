
<!DOCTYPE html>
<html lang="pt-br">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Controle de Séries</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    
</head>
<body>

    <div class="container">
        <div class="jumbotron border-0">
            <h1>Controle de Séries</h1>        
        </div>

        <a href="series/criar_serie.php" class="btn btn-dark mb-2"> Adicionar </a>
        
        <ul class="list-group">
        
        <?php // foreach($serie as $series): ?>
            
            <li class="list-group-item">Séries 1</li>
            <li class="list-group-item">Séries 2</li>
            <li class="list-group-item">Séries 3</li>

        <?php // endforeach ?>
        
        </ul>
    </div>

</body>
</html>
