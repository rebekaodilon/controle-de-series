
<!DOCTYPE html>
<html lang="pt-br">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Controle de Séries</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    
</head>
<body>

    <div class="container">
        <div class="jumbotron border-0">
            <h1>Adicionar Série</h1>        
        </div>

        <form id="formulario_criar" class="col-md-5 offset-md-3">

            <label for="nome" class="label-">Nome</label>
            <input type="text" class="form-control" name="nome" id="nome">

            <label for="temporada" class="label-">Temporada</label>
            <input type="text" class="form-control" name="temporada" id="temporada">

            <div style="text-align: right;">
                <input type="submit" class="btn btn-danger mt-2" value="Enviar">
            </div>
        </form>

        <a href=".." class="btn btn-dark mt-2"> Voltar </a>
    </div>

</body>
</html>
